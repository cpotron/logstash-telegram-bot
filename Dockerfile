FROM python:3.8-slim-buster

COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt

COPY ./notifier/ /app/notifier
WORKDIR /app
ENTRYPOINT [ "gunicorn", "notifier.run" ]