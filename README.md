# Logstash Telegram bot

This script lets you send formatted content through HTTP POST to your Telegram bot. Message content is managed thanks to Jinja2 templates.

## How to configure it

1. Create your environment variables file and edit it according to your setup
   ```
   cp .env.example .env
   ```
   - `TEMPLATE_PATH` : Path to the template directory
   - `CONFIG_PATH` : Path to the config file
   - `BOT_TOKEN` : Telegram bot token (<https://core.telegram.org/bots> for more details)
   - `CHAT_ID` : Telegram chat ID where your bot is and where you want to receive messages
   - `GUNICORN_CMD_ARGS` : Web server settings (<https://docs.gunicorn.org/en/stable/settings.html> for more details)
2. Edit `config.yaml` and add the number of endpoints you want. In the example, there is only one endpoint (`dummy`).
   ```yaml
   dummy:
      key: id
      default: raw.j2
   ```
   An endpoint must be defined with the following keys :
   - `key` : field in the request body used to determine what template will be used
   - `default` : fallback template file if no one is matching with the `key` provided above

## Setup

### Standalone (with Poetry)

You should use [Poetry](https://github.com/python-poetry/poetry) to install project dependencies.

```
poetry install
poetry run gunicorn notifier.run
```

### Docker

Basic docker-compose file below. `templates` directory and `config.yaml` file are both bind-mounted to preserve data when container is deleted. `settings.env` corresponds to `.env` file created in [this section](#how-to-configure-it).

```yaml
version: "3.7"
services:
  notifier:
    build: https://gitlab.com/cpotron/logstash-telegram-bot.git
    ports:
      - "8080:8080"
    volumes:
      - ./templates:/app/templates:ro
      - ./config.yaml:/app/config.yaml:ro
    env_file:
      - ./settings.env
```