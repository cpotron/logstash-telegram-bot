import time
from collections import defaultdict
from json import dumps
from os import getenv
from threading import Thread

from dotenv import load_dotenv
from emoji import emojize
from flask import Flask, request
from jinja2 import Environment, FileSystemLoader, Template, TemplateNotFound
from telegram import Bot
from telegram.utils.helpers import escape_markdown
from yaml import safe_load

from notifier.utils import get_nested_value

# Load .env file variables
load_dotenv()

# Create flask app
application = Flask(__name__)

# Create Telegram bot
bot = Bot(token=getenv("BOT_TOKEN"))
chat_id = getenv("CHAT_ID")
renderer = Environment(
    loader=FileSystemLoader(getenv("TEMPLATE_PATH")),
    trim_blocks=True,
    lstrip_blocks=True,
)

# Load configuration file
with open(getenv("CONFIG_PATH")) as config_file:
    config = safe_load(config_file)

# Check if configuration file is well-formatted
for log_type in config:
    if not all(key in config[log_type].keys() for key in ["key", "default"]):
        raise KeyError(f"Key missing in config file for {log_type} log type !")

# Format and send message to bot
def handle_bot_send(log_type, json_data):
    try:
        formatted_template = renderer.get_template(
            f"{log_type}/{get_nested_value(json_data, config[log_type]['key'].split(' -> '))}.j2"
        )
    except (TemplateNotFound, KeyError):
        formatted_template = renderer.get_template(
            f"{log_type}/{config[log_type]['default']}"
        )

    bot.send_message(
        chat_id=chat_id,
        text=emojize(
            formatted_template.render(
                content=json_data, escape=escape_markdown, print_json=dumps
            ),
            use_aliases=True,
        ),
        parse_mode="Markdown",
    )


# Map HTTP endpoint to receive logstash output message
@application.route("/<log_type>", methods=["POST"])
def handle_logstash_input(log_type):
    if log_type in config.keys():
        logstash_data = request.get_json()
        Thread(target=handle_bot_send, args=(log_type, logstash_data)).start()
        return ("OK", 200)
    else:
        return (f"Type {type} unknown !", 404)
