.PHONY: check
check:
	poetry run isort -rc -c notifier/
	poetry run black --check notifier/
	find notifier/ -iname "*.py" | xargs poetry run pylint --disable=C

.PHONY: format
format:
	poetry run isort -rc -y notifier/
	poetry run black notifier/

.PHONY: update
update:
	poetry update
	poetry export -f requirements.txt -o requirements.txt

.PHONY: build-docker-image
build-docker-image:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
	docker build --tag logs-notifier ./